# TolGitBot

> TolGitBot is a Tolgee Integration for Git projects to make sure that Tolgee and your Git repo are always sync: if you add a translation string during your dev, TolGit will update Tolgee. And TolGit will often check for new translations on Tolgee.

How TolGit works?
* You define where are the localized string in your repo ;
* You add TolGit to your repo (see above) and create a Gitlab Bot account

It works! 🥳🥳

This project is used by [Aktivisda](https://aktivisda.earth/) and [Memo-viewer](https://memo.fresque.earth/).

## Changelog

### v1.2.1. REVIEWED translations were ignored when we asked for TRANSLATED keys
2024-10-08

This is a really quick fix following v1.2.0. We have to explicitely ask Tolgee to return both REVIEWED and TRANSLATED keys.


### v1.2.0. Possibility to pull translated keys and not only reviewed
2024-10-07

v1.1.0 gave us the possibility to pull only "REVIEWED" translations but sometimes we want to pull "TRANSLATED" keys. It's now possible to define a new ENV VAR `TOLGEE_STATE` to set up the state we want: `TRANSLATED` or `REVIEWED`.


### v1.1.1. Fix bug when requesting langs unavailable on Tolgee
2024-10-07

I don't understand quite well where the trouble was but if you requested translations for `pt` on Tolgee and only `pt-BR` was available, you got a unhandled response.

Now fixed.

### v1.1.0. Only reviewed translations are pulled

Two new features:
* the cli checks if env variables are defined and raised errors if not;
* only reviewed translations are pulled on `npm run pull`

## Getting Started

### 1️⃣ Write code to tell TolGit where are your localized strings!

First, create a directory in your project, for example `.tolgee`, create a `package.json` following this example:

```json
{
    "name": "xxx-translate",
    "private": true,
    "type":"module",
    "scripts": {
        "pull": "node --env-file=.env node_modules/tolgitbot/cli.mjs pull -i tolgitinterface.js",
        "push": "node --env-file=.env node_modules/tolgitbot/cli.mjs push -i tolgitinterface.js"
    },
    "dependencies": {
        "commander": "^12.0.0",
        "flat": "^6.0.1"
    }
}
```

Then, you have to write a `tolgitinterface.js` script which export four functions:
* `pull` receives all the translations from Tolgee in a flatten mode.
Example:
    ```js
    export const pull = (translations) => {
        for (let const code in translations) {
            fs.writeFileSync(`${code}.json`, JSON.stringify(translations[code], null, 4));
        }
    }

    const translations = {
        "fr": {
            "my.key.one": "french translation"
        },
        "en": {
            "my.key.one": "second translation"
        }
    };
    pull(translations);
    ```
    Your job is to save these translations into your files using [Node file-system](https://nodejs.org/api/fs.html).
* `listI18nCodes` returns a list of all the `i18n` codes you have/want for your project. For example `["fr", "en"]` or `["fr-FR", "en-GB"]`.
Example:
    ```js
    export const listI18nCodes = () => {
        const langs = fs.readdirSync('i18n');
        const codes = [];
        for (let lang of langs) {
            const code = lang.slice(0, -5); // Remove .json at the end
            codes.push(code);
        }
        return codes;
    }
    ```
* `push` returns a _flatten_ object with all the translations you want to store in Tolgee **and** an object containing the list of Tolgee tags you want for each translation key.
Example:
    ```js
    export const push = () => {
        const translationsByCode = {};
        translationsByCode["fr"]["hello.world"] = "bonjour le monde";
        translationsByCode["en"]["hello.world"] = "hello world";
        tags["hello.world"] = ["message", "test"];
        return { translationsByCode, tags };
    }
    ```
* `config` returns an object used by TolgitBot for configuration:
    * `PUSH_ALL` = `true|false`. It tells to TolgitBot how to manage translations that already exist in Tolgee. If `PUSH_ALL` then Tolgit will push all local translations and can consequently overload some translations present on Tolgee. Else, Tolgit will push only new translations and will ignore all translations already on Tolgee.
    * `PULL_STATUS` = `translated|reviewed`: It tells to TolgitBot which translations should be imported to the project, all the translations? Only the reviewed?

### 2️⃣ Define local env variables

TolGitBot requires many variables. You can define each using `export MY_VARIABLE=...` in your terminal.
* `PROJECT_ID`: this is the project id of your project in Tolgee (you can find it directly on Tolgee in the url) ;
* `TOLGEE_URL`: with `https://` (ex. `https://tolgee.io`)
* `TOLGEE_TOKEN`: you can create it in Tolgee interface. Should have read and write rights to your repo

Once done, you can test your work using the `npm` commands:
* `npm install` to download `TolgitBot` dependency!
* `npm run push` to push your local translations into Tolgee ;
* `npm run pull` to pull Tolgee translations and write them into your project

### 3️⃣ Gitlab implementation

The idea is to execute the `pull` step regularly using a `scheduled pipeline`: this pull step will create a Merge Request (or directly a commit if you prefer).

On the other side, the `push` step can be executed for each commit on your main branch.

#### 🅰️ Import the Gitlab-ci jobs

First include `tolgie.gitlab-ci.yml` at the beginning of your `.gitlab-ci.yml` :
```yaml
include:
    - 'https://framagit.org/Marc-AntoineA/tolgitbot/-/raw/main/tolgee.gitlab-ci.yml'
```

Then, if you want to customize some job you can define them above. Here for ex., I define the variable `TOLGEE_PULL_USE_MR: false` in order to directly commit the translations to the project, and I execute a `git clone` before the main script.

```
translations-pull:
  variables:
    TOLGEE_PULL_USE_MR: false
  dependencies: []
  needs: []
  before_script:
    - git clone --depth 1 ... -b ...
```

#### 🅱️ Define some variables and create the bot



## CI Variables


* `TOLGEE_BOT`
* `TOLGEE_BOT_EMAIL`
* `TOLGIT_TOKEN` : this is a Gitlab Project Token Access with `write_repository` access.
* `TOLGEE_PULL_USE_MR` :true` or `false` if you want to create a merge request for all new translations
