'use strict';

import { program } from 'commander';
import { resolve } from "path";

const PROJECT_ID = process.env.TOLGEE_PROJECT_ID;
const TOLGEE_URL = process.env.TOLGEE_URL;

// Allowed values: TRANSLATED or REVIEWED
const TOLGEE_STATE = process.env.TOLGEE_STATE || 'REVIEWED';

function checkInstallation() {
    let valid = true;
    if (TOLGEE_URL === undefined) {
        console.warn('Please provide a variable env: TOLGEE_URL')
        valid = false;
    }
    if (PROJECT_ID === undefined) {
        console.warn('Please provide a variable env: TOLGEE_PROJECT_ID')
        valid = false;
    }
    if (process.env.TOLGEE_TOKEN === undefined) {
        console.warn('Please provide a variable env: TOLGEE_TOKEN')
        valid = false;
    }

    if (process.env.TOLGEE_STATE === undefined) {
        console.warn('TOLGEE_STATE is undefined, defaultValue used: ', TOLGEE_STATE)
    }
    return valid;
}

function flattenNestedObject(nestedObject, prefix, result) {
    for (const key in nestedObject) {
        const newPrefix = prefix === '' ? key : prefix + '.' + key
        if (typeof nestedObject[key] === 'object') {
            flattenNestedObject(nestedObject[key], newPrefix, result)
        } else {
            result[newPrefix] = nestedObject[key];
        }
    }
}

const fetchWithRetries = async(url, options, retryCount) => {
    try {
        return await fetch(url, options)
    } catch (error) {
        // if the retryCount has not been exceeded, call again
        if (retryCount > 0) {
            return fetchWithRetries(url, options, retryCount - 1)
        }
        // max retries exceeded
        throw error
    }
}

/**
 * 
 * Execute any Tolgee Request using the public REST API
 * 
 * @param {*} endpoint 
 * @param {*} method 
 * @param {*} body 
 * @returns 
 */
const tolgeeRequest = (endpoint, method, body) => {
    const url = `${TOLGEE_URL}/${endpoint}`;

    return new Promise((resolve, reject) => {
        fetchWithRetries(url, {
            method: method,
            headers: { 'Content-Type': 'application/json', 'X-API-Key': process.env.TOLGEE_TOKEN },
            body: method === 'GET' ? null : JSON.stringify(body),
        }, 3)
            .then((response) => {
                if (!response.ok) {
                    response.json().then((x) => {
                        if (!x.CUSTOM_VALIDATION)
                            console.error(x);
                        reject(x);
                    });
                    return;
                }
                return response.json();
            })
            .then((response) => resolve(response))
            .catch((error) => {
                console.error('Error in Tolgee request', endpoint, error);
            })
    });
};

/**
 *
 * Create a language on Tolgee for the current project
 *
 * @param {*} code
 * @param {*} name
 * @returns
 */
const createLanguage = (code, name) => {
    return new Promise((resolve) => tolgeeRequest(`v2/projects/${PROJECT_ID}/languages`, 'POST', {
        name: code === undefined ? name : code,
        originalName: code === undefined ? name : code,
        tag: code,
    }).then((x) => resolve(x)).catch(() => resolve()));
};

const updateOrCreateTranslation = (key, namespace, translations) => {
    return tolgeeRequest(`v2/projects/${PROJECT_ID}/translations`, 'POST', {
        key: key,
        namespace: namespace === '' ? undefined : namespace,
        translations: translations,
        languagesToReturn: [],
    });
};

/**
 * Request all the translation keys in the current project
 *
 * @returns
 */
const getAllKeys = () => {
    return tolgeeRequest(`v2/projects/${PROJECT_ID}/all-keys`, 'GET')
}

const addTag = (keyId, tag) => {
    return tolgeeRequest(`v2/projects/${PROJECT_ID}/keys/${keyId}/tags`, 'PUT', {
        name: tag,
    });
}

/**
 * Pull all projects translations from tolgee using the "get all translations" query
 * See: https://tolgee.io/api/get-all-translations
 *
 * @param {*} languages
 * @returns
 */
const pullTranslations = (languages) => {
    const url = `v2/projects/${PROJECT_ID}/translations/${languages.join(',')}`;
    return tolgeeRequest(url, 'GET', {});
};

const set = (object, path, value) => {
    const key = path[0];
    if (object[key] === undefined) {
        if (path.length === 1) {
            object[key] = value;
        } else {
            object[key] = {};
            set(object[key], path.slice(1), value);
        }
    } else {
        set(object[key], path.slice(1), value);
    }
}

/**
 *
 * Pull all reviewed translations from tolgee using the "get translations in project" query
 * See: https://tolgee.io/api/get-translations
 *
 * @param {*} languages
 */
const pullReviewedTranslations = (languages) => {
    const allPromises = [];
    for (const language of languages) {
        const filterState = [language,TOLGEE_STATE];

        // We also want REVIEWED translations if we ask for TRANSLATED ones
        if (TOLGEE_STATE === 'TRANSLATED')
            filterState.push([language,'REVIEWED']);

        const searchParams = new URLSearchParams({
            languages: [language],
            filterState: filterState,
            size: 10000
        });
        const url = `v2/projects/${PROJECT_ID}/translations?${searchParams.toString()}`

        allPromises.push(new Promise((resolve) => {
            tolgeeRequest(url).then((response) => {
                if (response._embedded === undefined) {
                    return resolve(undefined);
                }
                const keys = response._embedded.keys;

                const translations = {};
                for (const key of keys) {
                    const l = Object.keys(key.translations)[0]
                    if (l === undefined) continue; // it means that key.translations = {};
                    set(translations, [l, ...key.keyName.split('.').map((x) => x.toString())], key.translations[l].text);
                }
                resolve(translations)
            });
       }))
    }
    return new Promise((resolve) => {
        Promise.all(allPromises).then((allTranslations) => {
            const translations = {};
            for (const t of allTranslations) {
                if (t === undefined || Object.keys(t).length === 0) continue;
                const language = Object.keys(t)[0]
                translations[language] = t[language]
            }
            resolve(translations);
        });
    });
}


program.name('tolgee-cli').description('CLI to some tolgee sync utilities');

program
    .command('pull')
    .description('Pull strings from tolgee')
    .option('-i, --interface <type>', 'Path to your tolgitinterface.js')
    .action((options) => {
        import(resolve(options.interface)).then((TolGitInterface) => {
            if (!checkInstallation()) return;
            console.log('> Pull translations from Tolgee...')
            const codes = TolGitInterface.listI18nCodes();
            pullReviewedTranslations(codes).then((translations) => {
                TolGitInterface.pull(translations);
                console.log('   ... done')
            });
        });
    });

    program
    .command('push')
    .description('Push strings to tolgee')
    .option('-i, --interface <type>', 'Path to your tolgitinterface.js')
    .action((options) => {
        import(resolve(options.interface)).then((TolGitInterface) => {
            if (!checkInstallation()) return;
            console.log('> Push translations to Tolgee...')

            const codes = TolGitInterface.listI18nCodes();
            const requestExistingKeysPromise =
                process.env.TOLGEE_PUSH_ONLY_NEW_KEYS ?
                pullTranslations(codes) :
                new Promise((resolve) => resolve({}));

            // Existing translations: code, [key.key2]
            requestExistingKeysPromise.then((existingTranslationsbyCode) => {
                const flattenExistingTranslationsByCode = {};
                for (const code in existingTranslationsbyCode) {
                    flattenExistingTranslationsByCode[code] = {}
                    flattenNestedObject(existingTranslationsbyCode[code], '', flattenExistingTranslationsByCode[code])
                }

                let createLanguagesPromises = [];
                for (let code of codes) {
                    createLanguagesPromises.push(createLanguage(code, undefined));
                }
                Promise.all(createLanguagesPromises).then(() => {
                    const translationsByKey = {};
                    const { translationsByCode, tags } = TolGitInterface.push();
                    for (let code in translationsByCode) {
                        for (let key in translationsByCode[code]) {
                            // Filter translations already on tolgee
                            if (flattenExistingTranslationsByCode[code] !== undefined && flattenExistingTranslationsByCode[code][key] !== undefined) continue;
                            if (translationsByKey[key] === undefined) translationsByKey[key] = {};
                            translationsByKey[key][code] = translationsByCode[code][key];
                        }
                    }

                    const createKeyPromises = [];
                    for (let key in translationsByKey) {
                        createKeyPromises.push(updateOrCreateTranslation(key, '', translationsByKey[key]));
                    }
                    Promise.all(createKeyPromises).then(() => {
                        console.log('> Create tags')
                        getAllKeys().then((response) => {
                            const keys = response._embedded.keys;

                            const tagsPromises = [];
                            for (let key in tags) {
                                for (let tag of tags[key]) {
                                    let keyId = keys.find((x) => x.name === key).id
                                    tagsPromises.push(addTag(keyId, tag));
                                }
                            }
                            Promise.all(tagsPromises).then(() => {
                                console.log('... push done')
                            });
                        });
                    }).catch((err) => {
                        console.error('Error while creating key', err)
                    });
                });
            });
        });
    });


program.parse();

